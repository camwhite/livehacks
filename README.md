# Live Hacks

Coding livestreams over WebRTC

### Install the dependencies

```bash
yarn
```

### Start the backend

```bash
yarn workspace backend start
```

### Start the frontend in development mode (hot-code reloading, error reporting, etc.)

```bash
yarn workspace frontend run ssr:serve
```

### Build the frontend for production

```bash
yarn workspace frontend run ssr:build
```
