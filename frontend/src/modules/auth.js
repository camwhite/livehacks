export default {
  namespaced: true,
  state: () => ({
    token: null,
    currentUser: null
  }),
  actions: {
    async getCurrentUser({ commit }, payload) {
      const currentUser = await this.$http.get('users/me')
      commit('setCurrentUser', currentUser)
    },
    async getUser({ commit }, payload) {
      const user = await this.$http.get(`users/${payload}`)
      commit('setUser', user)
    }
  },
  mutations: {
    setToken(state, token) {
      state.token = token
    },
    setCurrentUser(state, currentUser) {
      state.currentUser = currentUser
    },
    setUser(state, user) {
      state.user = user
    }
  },
  getters: {
    isStreamer: state =>
      state.currentUser &&
      state.user &&
      state.currentUser.id === state.user.id
  }
}
