export default {
  namespaced: true,
  state: () => ({
    stream: {
      title: 'Untitled Hacking',
      description: 'Please update me'
    },
    messages: [],
    user: null,
    isLive: null,
    hasOverlay: true
  }),
  actions: {
    async getUser({ commit }, payload) {
      const user = await this.$http.get(`users/${payload}`)
      commit('setUser', user)
    },
    async getMessages({ commit }, payload) {
      const messages = await this.$http.get(
        `/messages/${payload}`,
        { query: { next: 0 } }
      )
      commit('setMessages', messages)
    }
  },
  mutations: {
    setStream(state, stream) {
      state.stream = stream
    },
    setOverlay(state, payload) {
      state.hasOverlay = payload
    },
    setViewers(state, viewers) {
      state.stream.viewers = viewers
    },
    setMessages(state, messages) {
      state.messages = messages
    },
    setUser(state, user) {
      state.user = user
    },
    setLive(state, payload) {
      state.isLive = payload
    },
    addMessage(state, message) {
      state.messages.push(message)
    },
    addMessages(state, messages) {
      state.messages.unshift(...messages)
    }
  }
}
