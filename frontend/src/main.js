import Vue from 'vue'
import App from './App.vue'
import NoSSR from 'vue-no-ssr'
import VueMeta from 'vue-meta'
import VueTimeago from 'vue-timeago'
import VueMasonry from 'vue-masonry-css'
import VueLazyload from 'vue-lazyload'
import createRouter from './router'
import createStore from './store'
import Modal from './components/Modal'
import * as components from './components'
import './assets/tailwind.css'

Vue.config.productionTip = false

Vue.use(VueMeta)
Vue.use(VueMasonry)
Vue.use(VueLazyload, {
  preLoad: 1.3,
  attempt: 1
})
Vue.use(VueTimeago, {
  name: 'Timeago',
  locale: 'en'
})

const registerProgrammatic = (Vue, component) => {
  const Programmatic = {
    open(params, parent = null) {
      const vm =
        typeof window !== 'undefined' && window.Vue
          ? window.Vue
          : Vue
      const Component = vm.extend(component)
      return new Component({
        el: document.createElement('div'),
        propsData: params,
        parent
      })
    }
  }
  const dollar = `$${component.name.toLowerCase()}`
  Vue.prototype[dollar] = Programmatic
}
for (let componentKey in components) {
  if (components[componentKey].programmatic) {
    registerProgrammatic(Vue, components[componentKey])
  }
}

Vue.component('no-ssr', NoSSR)

export default () => {
  const store = createStore()
  const router = createRouter()
  return new Vue({
    router,
    store,
    render: h => h(App)
  })
}
