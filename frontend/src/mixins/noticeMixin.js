export default {
  props: {
    message: {
      type: String,
      required: true
    },
    background: {
      type: String,
      required: true
    },
    color: {
      type: String,
      required: true
    },
    closable: {
      type: Boolean,
      default: false
    },
    duration: {
      default: 3000
    },
    handler: Function
  },
  data() {
    return {
      isActive: false
    }
  },
  computed: {
    queue() {
      return this.$store.state.noticeQueue
    }
  },
  methods: {
    showNotice() {
      if (this.queue > 0) {
        setTimeout(() => this.showNotice(), 250)
        return
      }
      this.isActive = true
      this.$store.commit('incrementQueue')
      if (this.duration) {
        setTimeout(() => {
          this.closeNotice()
        }, this.duration)
      }
    },
    closeNotice(evt) {
      this.isActive = false
      if (this.handler) {
        this.handler(true, evt)
      }
      this.$store.commit('decrementQueue')
      this.$destroy()
      this.$el.remove()
    }
  },
  beforeMount() {
    document.body.appendChild(this.$el)
  },
  mounted() {
    this.showNotice()
  }
}
