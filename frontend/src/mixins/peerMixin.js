import { mapState, mapGetters, mapMutations } from 'vuex'

export default {
  computed: {
    ...mapState({
      currentUser: state => state.auth.currentUser,
      user: state => state.users.user,
      isLive: state => state.users.isLive,
      stream: state => state.users.stream
    }),
    ...mapGetters(['isStreamer'])
  },
  data() {
    return {
      peerConnections: {},
      streamSrc: null,
      hasStream: null,
      isLoading: null
    }
  },
  methods: {
    ...mapMutations({
      setLive: 'users/setLive',
      setStream: 'users/setStream'
    }),
    getPc(id) {
      if (this.peerConnections[id]) {
        return this.peerConnections[id]
      }
      const servers = {
        iceServers: [
          { urls: 'stun:stun.ekiga.net' },
          { urls: 'stun:stun.fwdnet.net' },
          { urls: 'stun:stun.ideasip.com' },
          { urls: 'stun:stun.iptel.org' },
          { urls: 'stun:stun.rixtelecom.se' }
        ]
      }
      const pc = new RTCPeerConnection(servers)
      if (this.streamSrc) {
        this.streamSrc.getTracks().forEach(track => {
          pc.addTrack(track.clone(), this.streamSrc)
        })
      }
      pc.ontrack = ({ streams }) => {
        this.streamSrc = streams[0].clone()
        if (!this.isLive) {
          this.setLive(true)
          this.hasStream = true
        }
      }
      pc.onicecandidate = evt => {
        if (evt.candidate) {
          this.$socket.emit('signal', {
            by: this.socketId,
            to: id,
            ice: evt.candidate,
            type: 'ice'
          })
        }
      }
      this.peerConnections[id] = pc

      return pc
    },
    async makeOffer(id) {
      const pc = this.getPc(id)
      const sdp = await pc.createOffer()
      await pc.setLocalDescription(sdp)
      this.$socket.emit('signal', {
        type: 'sdp-offer',
        by: this.socketId,
        to: id,
        sdp
      })
    },
    onLeave(peerId) {
      if (this.peerConnections[peerId]) {
        const pc = this.getPc(peerId)
        pc.close()
        this.peerConnections[peerId] = null
      }
      if (this.user.id === peerId) {
        this.setLive(null)
        this.streamSrc = null
        this.hasStream = null
        Object.keys(this.peerConnections).forEach(pc => {
          this.peerConnections[pc].close()
          this.peerConnections[pc] = null
        })
      }
    },
    async handleSignal(signal) {
      const pc = this.getPc(signal.by)
      switch (signal.type) {
        case 'sdp-offer':
          console.log('Setting remote description by offer')
          await pc.setRemoteDescription(
            new RTCSessionDescription(signal.sdp)
          )
          const sdp = await pc.createAnswer()
          await pc.setLocalDescription(sdp)
          this.$socket.emit('signal', {
            by: signal.to,
            to: signal.by,
            sdp: sdp,
            type: 'sdp-answer'
          })
          break
        case 'sdp-answer':
          console.log('Setting remote description by answer')
          await pc.setRemoteDescription(
            new RTCSessionDescription(signal.sdp)
          )
          break
        case 'ice':
          if (signal.ice) {
            console.log('Adding ice candidates')
            pc.addIceCandidate(new RTCIceCandidate(signal.ice))
          }
          break
      }
    },
    async handleLeave() {
      if (this.streamSrc) {
        this.streamSrc.getTracks().forEach(track => track.stop())
      }
      if (this.isStreamer) {
        if (this.audioSrc) {
          this.audioSrc.getAudioTracks()[0].stop()
          this.audioSrc = null
        }
        if (this.streamSrc) {
          this.$socket.emit('end', {
            user: this.currentUser,
            data: this.stream
          })
        } else {
          this.$socket.emit('leave', this.stream)
        }
      } else {
        this.$socket.emit('leave', this.stream)
      }
      this.streamSrc = null
      this.hasStream = null
      this.setLive(null)
    },
    handleErrors(err) {
      this.$store.commit('setError', err)
    }
  }
}
