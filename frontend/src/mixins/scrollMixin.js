import Vue from 'vue'

export default {
  data() {
    return {
      offset: 0,
      busy: false,
      done: false
    }
  },
  methods: {
    onScroll(el = null) {
      const scrollEl = el || window
      const scrollHeight = el
        ? el.scrollHeight
        : document.documentElement.scrollHeight
      let bottomOfWindow, topOfWindow
      if (this.$options.name === 'home') {
        bottomOfWindow =
          parseInt(
            (scrollEl.scrollY + scrollEl.innerHeight).toFixed(0)
          ) === scrollHeight
      } else {
        topOfWindow =
          parseInt(scrollEl.scrollTop.toFixed(0)) === 0
      }
      if (bottomOfWindow && !this.done) {
        this.onLoadMore()
      } else if (topOfWindow && !this.done) {
        this.onLoadMore()
      }
    },
    async onLoadMore() {
      const opts = {}
      opts.params = { next: (this.offset += 20) }
      this.busy = true
      const data = await this.$http.get(this.endpoint, opts)
      this.busy = false
      if (data.length === 0) {
        this.done = true
        return
      }
      if (this.$options.name === 'home') {
        this.streams = [...this.streams, ...data]
      } else {
        const prevScrollHeight = this.$refs.messages.scrollHeight
        this.addMessages(data)
        Vue.nextTick(() => {
          const newScrollHeight =
            this.$refs.messages.scrollHeight - prevScrollHeight
          this.$refs.messages.scrollTop = newScrollHeight
        })
      }
    }
  }
}
