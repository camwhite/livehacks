workbox.routing.registerRoute(
  new RegExp('/uploads'),
  new workbox.strategies.StaleWhileRevalidate({
    cacheName: 'uploads',
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200]
      })
    ]
  })
)
workbox.routing.registerRoute(
  new RegExp('/api/v1'),
  new workbox.strategies.NetworkFirst({
    cacheName: 'api',
    plugins: [
      new workbox.cacheableResponse.Plugin({
        statuses: [0, 200]
      })
    ]
  })
)
self.addEventListener('fetch', evt => {
  const url = new URL(evt.request.url)
  if (
    url.origin !== location.origin ||
    evt.request.method !== 'GET'
  ) {
    return
  }
  const matches = caches.match(evt.request, {
    ignoreSearch: true
  })
  evt.respondWith(
    (async function() {
      try {
        if ((await matches) && /\./g.test(url.pathname)) {
          return matches
        }
        const response = await fetch(evt.request)
        if (!/\./g.test(url.pathname)) {
          const cache = await caches.open(
            workbox.core.cacheNames.precache
          )
          await cache.put(evt.request, response.clone())
        }
        return response
      } catch (err) {
        return matches
      }
    })()
  )
})
self.skipWaiting()
workbox.precaching.precacheAndRoute(self.__precacheManifest)
