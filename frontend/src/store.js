import Vue from 'vue'
import Vuex from 'vuex'
import uuid from 'uuid/v4'
import createLogger from 'vuex/dist/logger'
import router from '@/router'
import modules from '@/modules'

Vue.use(Vuex)
const plugins = []
if (process.env.NODE_ENV !== 'production' && !process.server) {
  plugins.push(createLogger())
}

export default () => {
  return new Vuex.Store({
    state: () => ({
      noticeQueue: 0,
      error: null
    }),
    mutations: {
      incrementQueue(state) {
        state.noticeQueue++
      },
      decrementQueue(state) {
        state.noticeQueue--
      },
      setError(state, payload) {
        state.error = payload
      }
    },
    actions: {
      async onHttpRequest(store, { router }) {
        if (store.state.auth.token) {
          await store.dispatch('auth/getCurrentUser')
        }
      },
      _updateUser({ state, commit }, updated) {
        const { currentUser } = state.auth
        const { user } = state.users
        if (user && user.id === updated.id) {
          commit('users/setUser', updated)
        }
        if (currentUser && currentUser.id === updated.id) {
          commit('auth/setCurrentUser', updated)
        }
      },
      _updateStream({ state, commit }, updated) {
        commit('users/setStream', {
          ...state.users.stream,
          ...updated
        })
      },
      _viewerCount({ commit }, viewers) {
        commit('users/setViewers', viewers)
      }
    },
    getters: {
      isStreamer: state =>
        state.auth.currentUser &&
        state.users.user &&
        state.auth.currentUser.id === state.users.user.id
    },
    plugins,
    modules
  })
}
