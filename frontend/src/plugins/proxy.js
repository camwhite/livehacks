import { createProxyMiddleware } from 'http-proxy-middleware'

export default {
  install(app) {
    app.use(
      '/api',
      createProxyMiddleware({
        target: 'http://localhost:9001',
        changeOrigin: true
      })
    )
    app.use(
      '/uploads',
      createProxyMiddleware({
        target: process.env.CDN_URL,
        changeOrigin: true
      })
    )
    app.use(
      '/socket.io',
      createProxyMiddleware({
        target: 'http://localhost:9001',
        ws: true,
        logLevel: 'silent'
      })
    )
  }
}
