import Vue from 'vue'
import VueAnalytics from 'vue-analytics'

export default {
  beforeReady({ router }) {
    Vue.use(VueAnalytics, {
      id: 'UA-131007034-2',
      router,
      debug: {
        sendHitTask: process.env.NODE_ENV !== 'development'
      }
    })
  }
}
