export default {
  beforeCreate({ store, router }) {
    router.beforeEach((to, from, next) => {
      if (to.matched.some(record => record.meta.requriesAuth)) {
        if (store.state.auth.token) {
          next()
        } else {
          next('/')
        }
      } else {
        next()
      }
    })
  }
}
