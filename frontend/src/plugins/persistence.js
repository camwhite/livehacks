import createPersistedState from 'vuex-persistedstate'

export default {
  ready({ store }) {
    if (process.client) {
      createPersistedState({
        paths: ['isSideNavCollapsed']
      })(store)
    }
  }
}
