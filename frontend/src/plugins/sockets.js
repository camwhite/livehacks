import Vue from 'vue'
import VueSocketio from 'vue-socket.io'
import * as io from 'socket.io-client'

export default {
  beforeStart({ store }) {
    const options = {
      transports: ['websocket', 'polling']
    }
    const { token } = store.state.auth
    if (token) {
      options.query = { token }
    }
    Vue.use(
      new VueSocketio({
        connection: io.connect('', options),
        vuex: {
          store,
          actionPrefix: '_'
        }
      })
    )
  }
}
