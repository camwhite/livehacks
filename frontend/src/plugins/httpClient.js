import Vue from 'vue'
import axios from 'axios'

export default {
  beforeCreate({ store, req, res, redirect, error }, inject) {
    if (!process.client) {
      const { token } = req.cookies
      if (token) {
        store.commit('auth/setToken', token)
      } else {
        store.commit('auth/setToken', null)
        store.commit('auth/setCurrentUser', null)
      }
    } else {
      const Cookies = require('js-cookie')
      const token = Cookies.get('token') || null
      store.commit('auth/setToken', token)
    }

    const httpClient = axios.create()
    httpClient.defaults.baseURL = process.client
      ? '/api/v1'
      : process.env.VUE_APP_DOMAIN + '/api/v1'
    httpClient.interceptors.request.use(request => {
      const { token } = store.state.auth
      if (token) {
        // With our token present
        // we add it to the auth header with the bearer strategy
        request.headers['Authorization'] = `Bearer ${token}` // @TODO move into `axios.setToken` to decrease overhead
      }
      return request
    })
    httpClient.interceptors.response.use(
      response => {
        return response
      },
      err => {
        // error interceptor
        if (err.response) {
          if (err.response.status === 401) {
            store.commit('auth/setToken', null)
            store.commit('auth/setCurrentUser', null)
            if (!process.client) {
              res.setHeader(
                'Set-Cookie',
                `token=; Path=/; Max-Age=${Date.now()};`
              )
            } else {
              require('js-cookie').set('token', '', {
                path: '',
                expires: Date.now()
              })
            }
            redirect('/')
          }
          if (process.client) {
            store.commit('setError', err.response.data)
          } else {
            error(err.response.data, err.response.status)
          }
        }
        return Promise.reject(error)
      }
    )

    const $http = {
      // logout a user
      async logout() {
        // On client side: remove cookie with token
        store.commit('auth/setToken', null)
        store.commit('auth/setCurrentUser', null)
        require('js-cookie').set('token', '', { path: '' })
      },

      // upload an image to imgur
      async upload(blob, opts) {
        const instance = axios.create()
        const { data } = await instance.post('/uploads', blob, {
          ...opts
        })
        return data
      },

      // get something
      async get(uri, opts) {
        const { data } = await httpClient.get(uri, { ...opts })
        return data
      },

      // post something
      async post(uri, body, opts) {
        const { data } = await httpClient.post(uri, body, opts)
        return data
      },

      // post something
      async put(uri, body) {
        const { data } = await httpClient.put(uri, body)
        return data
      },

      // delete something
      async del(uri, body) {
        const { data } = await httpClient.delete(uri, body)
        return data
      }
    }

    inject('http', $http)
  }
}
