import Vue from 'vue'
import marked from 'marked'
import emoji from 'node-emoji'
import * as highlight from 'highlight.js'

export default {
  beforeStart() {
    Vue.prototype.$markdown = function(markdown) {
      const replacer = match => emoji.emojify(match)
      markdown = markdown
        .replace(/(:.*:)/g, replacer)
        .replace(
          /\[ \]*/g,
          emoji.emojify(':white_large_square:')
        )
        .replace(
          /\[x\]*/g,
          emoji.emojify(':ballot_box_with_check:')
        )
      return marked(markdown, {
        table: true,
        gfm: true,
        breaks: true,
        sanitize: true,
        highlight: code => {
          return highlight.highlightAuto(code).value
        }
      })
    }
  }
}
