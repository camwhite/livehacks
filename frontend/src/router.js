import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default () => {
  return new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
      {
        path: '/',
        component: () =>
          import(
            /* webpackchunkname: "home" */ './views/Home.vue'
          )
      },
      {
        path: '/login',
        name: 'login',
        meta: { guest: true },
        component: () =>
          import(
            /* webpackchunkname: "login" */ './views/Login.vue'
          )
      },
      {
        path: '/channel/:id',
        name: 'channel',
        meta: { guest: true },
        component: () =>
          import(
            /* webpackChunkName: "channel" */ './views/Channel.vue'
          )
      },
      {
        path: '/channel/:id/stats',
        name: 'stats',
        meta: { guest: true },
        component: () =>
          import(
            /* webpackChunkName: "stats" */ './views/Stats.vue'
          )
      },
      {
        path: '/chat/:id',
        name: 'chat',
        meta: { guest: true },
        component: () =>
          import(
            /* webpackChunkName: "chat" */ './views/Chat.vue'
          )
      },
      {
        path: '/about',
        name: 'about',
        meta: { guest: true },
        component: () =>
          import(
            /* webpackChunkName: "about" */ './views/About.vue'
          )
      },
      {
        path: '*',
        name: '404',
        meta: { guest: true },
        component: () =>
          import(
            /* webpackChunkName: "404" */ './views/FourOhFour.vue'
          )
      }
    ]
  })
}
