export default {
  //imports: [
  //{
  //src: 'webrtc-adapter',
  //ssr: false
  //}
  //],
  plugins: [
    '@uvue/core/plugins/errorHandler',
    [
      '@uvue/core/plugins/vuex',
      {
        onHttpRequest: true,
        fetch: false
      }
    ],
    '@uvue/core/plugins/asyncData',
    '@/plugins/httpClient',
    '@/plugins/guards',
    '@/plugins/markdownParser',
    '@/plugins/persistence',
    '@/plugins/serviceWorker',
    '@/plugins/sockets'
  ],
  transpileDependencies: [/carbon/g]
}
