import path from 'path'

export default {
  plugins: [
    '@uvue/server/plugins/gzip',
    [
      '@uvue/server/plugins/static',
      {
        directory: 'dist',
        options: {
          immutable: true,
          maxAge: '1y',
          setHeaders(res, path) {
            if (
              /service-worker\.js/.test(path) ||
              /ssr\.html/.test(path)
            ) {
              res.setHeader('Cache-Control', 'public, max-age=0')
              res.setHeader('Expires', '-1')
              res.setHeader('Pragma', 'no-cache')
            }
          }
        }
      }
    ],
    [
      '@uvue/server/plugins/cookie',
      {
        secret:
          'b45d956f3c7716650984cf42a1cdbd1dc73ac0a378cf2b733cf30109fd74ca4c0beebde60ca6b787'
      }
    ],
    [
      '@uvue/server/plugins/serverError',
      {
        path: path.join(__dirname, '..', 'backend', 'error.html')
      }
    ],
    './src/plugins/proxy'
    //'@uvue/server/plugins/modernBuild'
  ]
}
