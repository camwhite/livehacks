const path = require('path')

module.exports = {
  productionSourceMap: false,
  css: {
    loaderOptions: {
      sass: {
        prependData: `
          $main: #f1f3f6;
        `
      }
    }
  },
  devServer: {
    host: '0.0.0.0'
  },
  pwa: {
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swDest: 'service-worker.js',
      swSrc: path.join('src', 'sw.js'),
      templatedUrls: {
        '/': '/uvue/spa.html'
      }
    }
  }
}
