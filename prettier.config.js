module.exports = {
  printWidth: 65,
  bracketSpacing: true,
  trailingComma: 'none',
  semi: false,
  singleQuote: true
}
