exports.up = async function(knex) {
  await knex.schema.table('users', table => {
    table.string('ip_address')
  })

  return Promise.resolve()
}

exports.down = async function(knex) {
  await knex.schema.table('users', table => {
    table.dropColumn('ip_address')
  })

  return Promise.resolve()
}
