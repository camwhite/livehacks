const updateFn = `
CREATE OR REPLACE FUNCTION on_update_timestamp()
RETURNS trigger AS $$
BEGIN
  NEW.updated_at = now();
  RETURN NEW;
END;
$$ language 'plpgsql';
`
exports.up = async function(knex) {
  return knex.schema.raw(updateFn)
}

exports.down = async function(knex) {
  knex.schema.raw(`DROP FUNCTION on_update_timestamp`)
  return Promise.resolve()
}
