const { updateTrigger } = require('../knexfile.js')

const alter = `ALTER TABLE "users" ADD COLUMN "post_text" TSVECTOR`
const update = `UPDATE "users" SET "post_text" = to_tsvector('english', "email" || "handle")`
const index = `CREATE INDEX post_search_idx_users ON "users" USING gin("post_text")`
const trigger = `CREATE TRIGGER post_vector_update BEFORE INSERT OR UPDATE ON "users" FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger("post_text", 'pg_catalog.english', email, handle)`

exports.up = async function(knex) {
  await knex.schema
    .createTable('users', table => {
      table
        .uuid('id')
        .defaultTo(knex.raw('uuid_generate_v4()'))
        .primary()
      table.string('email').unique()
      table.string('handle')
      table.string('avatar')
      table.timestamps(true, true)
    })
    .raw(alter)
    .raw(update)
    .raw(index)
    .raw(trigger)
    .raw(updateTrigger('users'))

  return Promise.resolve()
}

exports.down = async function(knex) {
  await knex.schema.dropTable('users')

  return Promise.resolve()
}
