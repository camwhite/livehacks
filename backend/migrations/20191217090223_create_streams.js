const { updateTrigger } = require('../knexfile.js')

const alter = `ALTER TABLE "streams" ADD COLUMN "post_text" TSVECTOR`
const update = `UPDATE "streams" SET "post_text" = to_tsvector('english', "description" || "title")`
const index = `CREATE INDEX post_search_idx_streams ON "streams" USING gin("post_text")`
const trigger = `CREATE TRIGGER post_vector_update BEFORE INSERT OR UPDATE ON "streams" FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger("post_text", 'pg_catalog.english', description, title)`

exports.up = async function(knex) {
  await knex.schema
    .createTable('streams', table => {
      table.increments('id').primary()
      table.text('title')
      table.text('description')
      table.text('preview')
      table
        .uuid('user_id')
        .references('id')
        .inTable('users')
      table.timestamps(true, true)
      table.timestamp('deleted_at')
    })
    .raw(alter)
    .raw(update)
    .raw(index)
    .raw(trigger)
    .raw(updateTrigger('streams'))

  return Promise.resolve()
}

exports.down = async function(knex) {
  await knex.schema.dropTable('streams')

  return Promise.resolve()
}
