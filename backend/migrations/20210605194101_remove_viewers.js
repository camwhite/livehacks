exports.down = async function(knex) {
  await knex.schema.createTable('viewers', table => {
    table.increments('id').primary()
    table.string('ip_adress')
    table
      .uuid('room_id')
      .references('id')
      .inTable('users')
    table
      .uuid('user_id')
      .references('id')
      .inTable('users')
    table.timestamp('deleted_at')
  })

  return Promise.resolve()
}

exports.up = async function(knex) {
  await knex.schema.dropTable('viewers')

  return Promise.resolve()
}
