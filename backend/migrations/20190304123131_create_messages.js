const { updateTrigger } = require('../knexfile.js')

const alter = `ALTER TABLE "messages" ADD COLUMN "post_text" TSVECTOR`
const update = `UPDATE "messages" SET "post_text" = to_tsvector('english', "text")`
const index = `CREATE INDEX post_search_idx_messages ON "messages" USING gin("post_text")`
const trigger = `CREATE TRIGGER post_vector_update BEFORE INSERT OR UPDATE ON "messages" FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger("post_text", 'pg_catalog.english', text)`

exports.up = async function(knex) {
  await knex.schema
    .createTable('messages', table => {
      table.increments('id').primary()
      table.text('text')
      table
        .boolean('guest')
        .notNull()
        .defaultsTo(true)
      table.json('attachments')
      table
        .uuid('user_id')
        .references('id')
        .inTable('users')
      table
        .uuid('room_id')
        .references('id')
        .inTable('users')
      table.timestamps(true, true)
    })
    .raw(alter)
    .raw(update)
    .raw(index)
    .raw(trigger)
    .raw(updateTrigger('messages'))

  return Promise.resolve()
}

exports.down = async function(knex) {
  await knex.schema.dropTable('messages')

  return Promise.resolve()
}
