const users = [
  {
    handle: 'guest',
    email: 'guest@livehacks.tv'
  },
  {
    handle: 'testuser',
    email: 'test@test.com'
  }
]

exports.seed = async function(knex) {
  await knex('users').del()
  await knex('users').insert(users)
}
