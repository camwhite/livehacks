import moment from 'moment'

export const isProd = process.env.NODE_ENV === 'production'
export const domain =
  process.env.DOMAIN || 'http://localhost:8080'
export const jwtSecret = process.env.JWT_SECRET || 'foobarbaz'
export const jwtExpiration = () =>
  new moment().add(process.env.JWT_EXPIRATION || 8, 'h').unix()
export const knexOptions = {
  client: 'pg',
  version: '12.2',
  debug: false,
  connection: {
    host: process.env.POSTGRES_HOST,
    user: process.env.POSTGRES_USER || 'postgres',
    password: process.env.POSTGRES_PASSWORD,
    database: process.env.POSTGRES_DB || 'livehacks-dev'
  }
}
export const mg = {
  key: '532780b78dd2ad41813a55fb8f0c9cdf-1b6eb03d-c8c433f2',
  domain: 'mg.livehacks.tv'
}
export const OPENAI_API_KEY =
  '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'

export { default as emailTemplate } from './email'
