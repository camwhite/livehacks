import app from './index.js'

const port = process.env.PORT || 9001
const start = async () => {
  try {
    await app.listen(port)
  } catch (err) {
    console.error(err)
  }
}

;(async () => {
  await start()
})()
