export default class User {
  constructor(knex) {
    this.knex = knex
  }
  async show(id) {
    const [user] = await this.knex('users')
      .select(
        'id',
        'email',
        'handle',
        'avatar',
        'created_at',
        'updated_at'
      )
      .where('id', id)
    if (!user) {
      const err = new Error('Not Found :(')
      err.statusCode = 404
      throw err
    }
    return user
  }
  async update(update, id) {
    const [updated] = await this.knex('users')
      .where({ id })
      .update(update)
      .returning('*')
    return updated
  }
}
