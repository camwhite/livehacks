import moment from 'moment'

export default class Stat {
  constructor({ knex }) {
    this.knex = knex
    this.selections = [
      'id',
      'title',
      'description',
      'preview',
      'user_id',
      'created_at',
      'updated_at',
      'deleted_at'
    ]
  }
  model() {
    return this.knex('streams')
  }
  async list(id) {
    const date = new moment().subtract(30, 'd')
    const listed = await this.model()
      .select(
        'streams.id',
        'streams.title',
        'streams.description',
        'streams.preview',
        'streams.created_at',
        'streams.updated_at',
        'streams.deleted_at'
      )
      .where('streams.user_id', id)
      .andWhere('streams.created_at', '>=', date.toISOString())
      .whereNotNull('streams.deleted_at')
      .orderBy('streams.created_at', 'desc')
    return listed
  }
  async show(id) {
    const [shown] = await this.model()
      .select(
        'streams.id',
        'streams.title',
        'streams.description',
        'streams.preview',
        'streams.created_at',
        'streams.updated_at',
        'streams.deleted_at',
        this.knex.raw(
          `json_build_object('id', users.id, 'handle', users.handle, 'avatar', users.avatar, 'created_at', users.created_at) as user`
        )
      )
      .leftJoin('users', 'streams.user_id', '=', 'users.id')
      .where('streams.user_id', id)
      .orderBy('streams.created_at', 'desc')
      .limit(1)
      .groupBy('streams.id', 'users.id')

    let viewers = await this.redis.hget('viewers', id)
    viewers = JSON.parse(viewers)
    if (shown) {
      shown.viewers = viewers
    }

    return shown
  }

  async all() {
    const [totalStreams] = await this.knex('streams').count()
    const [totalUsers] = await this.knex('users').count()
    const [currentlyLiveUsers] = await this.knex('streams')
      .where({ deleted_at: null })
      .count()
    const [totalMessages] = await this.knex('messages').count()

    return {
      totalStreams: totalStreams.count,
      totalUsers: totalUsers.count,
      currentlyLiveUsers: currentlyLiveUsers.count,
      totalMessages: totalMessages.count
    }
  }
}
