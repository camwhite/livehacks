import puppeteer from 'puppeteer'
import { isProd } from '../../config'

let url
const tryScrapeMeta = async (page, selector, type) => {
  return await page.evaluate(
    ({ selector, type }) => {
      const tags = Array.from(
        document.querySelectorAll(selector)
      )
      const normalizeName = ({ attributes }) => {
        const { property, name, href } = attributes
        if (property) {
          return property.value.split(':').pop()
        }
        if (name) {
          return name.value
        }
        if (href) {
          return 'href'
        }
      }
      return tags
        .map(tag => {
          const tmp = {}
          const name = normalizeName(tag)
          tmp[name] = tag[type]
          return tmp
        })
        .reduce((a, b) => {
          const tmp = Object.assign(a, b)
          return tmp
        }, {})
    },
    { selector, type }
  )
}

const checkProperties = async (data, page) => {
  if (!data.title) {
    const title = await page.title()
    data = { ...data, title }
  }
  if (!data.image) {
    const { href } = await tryScrapeMeta(
      page,
      'link[rel="shortcut icon"]',
      'href'
    )
    const image = href
    data = { ...data, image }
  }
  if (!data.description) {
    const description = await tryScrapeMeta(
      page,
      'meta[name="description"]',
      'content'
    )
    data = { ...data, ...description }
  }
  if (!data.url) {
    data = { ...data, url }
  }
  return data
}

export const unfurl = async query => {
  url = query // hoist the url query

  const options = isProd
    ? {
        executablePath: '/usr/bin/chromium-browser',
        args: ['--no-sandbox']
      }
    : {}
  const browser = await puppeteer.launch(options)
  const page = await browser.newPage()

  await page.goto(url, { waitUntil: 'domcontentloaded' })

  let ogData = await tryScrapeMeta(
    page,
    'meta[property^="og:"]',
    'content'
  )
  if (Object.keys(ogData).length > 0) {
    ogData = await checkProperties(ogData, page)
  } else {
    const page = await browser.newPage()
    await page.setUserAgent('facebookexternalhit') // try facebooks user agent
    await page.goto(url, { waitUntil: 'domcontentloaded' })
    ogData = await tryScrapeMeta(
      page,
      'meta[property^="og:"]',
      'content'
    )
    ogData = await checkProperties(ogData, page)
    if (Object.keys(ogData).length === 0) {
      const err = new Error(
        'Sorry this link does not have enough info'
      )
      throw err
    }
  }
  await browser.close()
  return ogData
}

export default class Message {
  constructor(knex) {
    this.knex = knex
  }
  async create(message) {
    const newMessage = {
      ...message,
      user_id: message.user ? message.user.id : undefined
    }
    delete newMessage.user
    const [created] = await this.knex('messages')
      .insert(newMessage)
      .returning([
        'created_at',
        'updated_at',
        'id',
        'attachments',
        'user_id'
      ])
    if (message.user) {
      const { handle } = message.user
      message.handle = handle
    }
    message = { ...message, ...created }
    return message
  }
  async update(update, { id }) {
    const [updated] = await this.knex('messages')
      .where({ id })
      .update(update)
      .returning('*')
    return updated
  }
  async list(id, next) {
    const listed = await this.knex
      .select(
        'messages.id',
        'messages.text',
        'users.handle',
        'messages.user_id',
        'messages.created_at',
        'messages.updated_at',
        'messages.attachments'
      )
      .from('messages')
      .leftJoin('users', 'messages.user_id', '=', 'users.id')
      .where('messages.room_id', id)
      .orderBy('messages.created_at', 'desc')
      .limit(20)
      .offset(next)
    return listed.reverse()
  }
}
