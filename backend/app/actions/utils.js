import xss from 'xss'

export function sanitize(request, reply, next) {
  const { body } = request
  if (!body) {
    next()
  } else {
    const keys = Object.keys(body)
    for (const key of keys) {
      if (typeof body[key] === 'string') {
        request.body[key] = xss(body[key])
      }
    }
    next()
  }
}
export const uuidRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/i
export const testUuid = (request, reply, next) => {
  if (!request.params.id) return next()
  if (!uuidRegex.test(request.params.id)) {
    const err = new Error('Bad request :P')
    err.statusCode = 400
    reply.status(err.statusCode).send(err)
  } else {
    next()
  }
}
export const getBot = async knex => {
  const [user] = await knex('users')
    .select(
      'id',
      'email',
      'handle',
      'avatar',
      'created_at',
      'updated_at'
    )
    .where({ email: 'hax@livehacks.tv' })
  return user
}
