import axios from 'axios'

export default class Bot {
  constructor(gpt3, hax) {
    this.gpt3 = gpt3
    this.hax = hax
  }
  async generate(message) {
    const prompt = `
JavaScript chatbot


You: How do I combine arrays?
JavaScript chatbot: You can use the concat() method.
You: ${message.text}
JavaScript chatbot`

    const { data } = await this.gpt3.complete({
      prompt,
      engine: 'davinci',
      temperature: 0.3,
      maxTokens: 60,
      topP: 1,
      presencePenalty: 0,
      frequencyPenalty: 0.5,
      stream: false,
      stop: ['You:']
    })
    message.text = data.choices[0].text.replace(':', '').trim()
    message.user = this.hax
    delete message.handle

    return message
  }
  async taskord({ body, query }) {
    const task = await axios.post(query.webhook, body)
    return task
  }
}
