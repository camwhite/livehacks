import mailgun from 'mailgun-js'
import {
  domain,
  isProd,
  jwtExpiration,
  emailTemplate,
  mg
} from '../../config/index.js'

const emails = mailgun({ apiKey: mg.key, domain: mg.domain })
const userSelections = [
  'id',
  'email',
  'avatar',
  'handle',
  'created_at',
  'updated_at'
]

export const verifyJwt = async function(request, reply, next) {
  const { authorization } = request.headers
  const { knex, jwt } = this
  if (!authorization) {
    return next(new Error('No token'))
  }
  const [bearer, token] = authorization.split(' ')
  try {
    const decoded = await jwt.verify(token)
    const [user] = await knex('users')
      .select(userSelections)
      .where({ id: decoded.sub })
    if (!user) {
      return next(new Error('Token not valid'))
    }
    request.user = user
    next()
  } catch (err) {
    return next(err)
  }
}

export default class Auth {
  constructor(knex, jwt) {
    this.knex = knex
    this.jwt = jwt
  }
  async sendMail(email, socketId) {
    const accessToken = await this.jwt.sign({
      sub: email,
      aud: domain,
      iss: 'livehacks.tv',
      exp: jwtExpiration(),
      socketId
    })
    const loginUrl = `${domain}/login?access_token=${encodeURIComponent(
      accessToken
    )}`
    if (!isProd) {
      console.log(loginUrl)
    }
    const data = {
      from: '<no-reply@livehacks.tv>',
      to: email,
      subject: 'Sign In to Livehacks',
      html: emailTemplate(loginUrl)
    }
    return new Promise((resolve, reject) => {
      emails.messages().send(data, (err, body) => {
        if (err) reject(err)
        resolve(body)
      })
    })
  }
  async authenticate(accessToken, ipAddress) {
    const { sub, socketId } = await this.jwt.verify(accessToken)
    const [user] = await this.knex('users')
      .select('email')
      .where({ email: sub })
    let authenticated
    if (user) {
      authenticated = await this.signin({
        email: sub
      })
    } else {
      authenticated = await this.signup(
        {
          email: sub
        },
        ipAddress
      )
    }
    authenticated.socketId = socketId
    return authenticated
  }
  async signup(user, ipAddress) {
    user.ip_address = ipAddress
    const [currentUser] = await this.knex('users')
      .insert(user)
      .returning(['id', 'email', 'created_at'])
    const token = await this.jwt.sign({
      sub: currentUser.id,
      aud: domain,
      iss: 'livehacks.tv',
      exp: jwtExpiration()
    })
    return { currentUser, token }
  }
  async signin({ email }) {
    const [currentUser] = await this.knex('users')
      .select(userSelections)
      .where({ email })
    if (!currentUser) {
      const err = new Error('Email not registered :(')
      err.statusCode = 404
      throw err
    }
    const token = await this.jwt.sign({
      sub: currentUser.id,
      aud: domain,
      iss: 'livehacks.tv',
      exp: jwtExpiration()
    })
    return {
      currentUser,
      token
    }
  }
}
