import { uuidRegex } from './utils.js'

export default class Stream {
  constructor({ knex, redis }) {
    this.knex = knex
    this.redis = redis
    this.selections = [
      'id',
      'title',
      'description',
      'preview',
      'user_id',
      'created_at',
      'updated_at',
      'deleted_at'
    ]
  }
  model() {
    return this.knex('streams')
  }
  async create(stream, user) {
    const creation = {
      title: stream.title,
      description: stream.description,
      preview: stream.preview,
      user_id: user.id
    }
    const [created] = await this.model()
      .insert(creation)
      .returning(this.selections)
    created.user = user
    return created
  }
  async update(update, id) {
    delete update.user
    delete update.deleted_at
    const [updated] = await this.model()
      .where({ id: update.id || id })
      .update(update)
      .returning(this.selections)
    return updated
  }
  async softDel(id) {
    const [deleted] = await this.model()
      .where({ user_id: id })
      .andWhere({ deleted_at: null })
      .update({ deleted_at: this.knex.fn.now() })
      .returning(this.selections)
    return deleted
  }
  async addViewer(room, userId, ipAddress) {
    if (!uuidRegex.test(userId)) {
      userId = null
    }
    let viewers = await this.redis.hget('viewers', room)
    if (!viewers) {
      await this.redis.hset('viewers', room, '[]')
      return []
    }
    viewers = JSON.parse(viewers)

    const currentViewer = viewers.find(
      viewer => viewer.ip_address === ipAddress
    )
    if (currentViewer) {
      return viewers
    }

    const [user] = await this.knex('users')
      .select(
        'users.id',
        'users.handle',
        'users.avatar',
        'users.created_at',
        'users.ip_address'
      )
      .where({ id: userId })
    if (user) {
      viewers.push(user)
    } else {
      viewers.push({
        handle: 'guest',
        ip_address: ipAddress
      })
    }

    await this.redis.hset(
      'viewers',
      room,
      JSON.stringify(viewers)
    )

    return viewers.map(({ ip_address, ...rest }) => rest)
  }
  async removeViewer(room, userId, ipAddress) {
    if (!uuidRegex.test(userId)) {
      userId = null
    }
    let viewers = await this.redis.hget('viewers', room)
    viewers = JSON.parse(viewers)

    const index = viewers.findIndex(
      viewer => viewer.ip_address === ipAddress
    )
    viewers.splice(index, 1)
    await this.redis.hset(
      'viewers',
      room,
      JSON.stringify(viewers)
    )

    return viewers.map(({ ip_address, ...rest }) => rest)
  }
  async all(next) {
    const streams = await this.model()
      .select(
        'streams.id',
        'streams.title',
        'streams.description',
        'streams.preview',
        'streams.created_at',
        'streams.updated_at',
        'streams.deleted_at',
        this.knex.raw(
          `json_build_object('id', users.id, 'handle', users.handle, 'avatar', users.avatar, 'created_at', users.created_at) as user`
        )
      )
      .leftJoin('users', 'streams.user_id', '=', 'users.id')
      .orderBy('streams.created_at', 'desc')
      .offset(next)
      .limit(20)
      .groupBy('streams.id', 'users.id')

    for (const [index, stream] of streams.entries()) {
      if (!stream.deleted_at) {
        let viewers = await this.redis.hget(
          'viewers',
          stream.user.id
        )
        if (!viewers) break
        viewers = JSON.parse(viewers).map(
          ({ ip_address, ...rest }) => rest
        )
        streams[index].viewers = viewers
      }
    }
    return streams
  }
  async list(id, next) {
    const listed = await this.model()
      .select(
        'streams.id',
        'streams.title',
        'streams.description',
        'streams.preview',
        'streams.created_at',
        'streams.updated_at',
        'streams.deleted_at',
        this.knex.raw(
          `json_build_object('id', users.id, 'handle', users.handle, 'avatar', users.avatar, 'created_at', users.created_at) as user`
        )
        //this.knex.raw(
        //`coalesce(json_agg(viewers) FILTER (WHERE viewers.deleted_at IS NULL), '[]'::json) as viewers`
        //)
      )
      .leftJoin('users', 'streams.user_id', '=', 'users.id')
      //.leftJoin('viewers', 'viewers.room_id', '=', 'users.id')
      .where('streams.user_id', id)
      .orderBy('streams.created_at', 'desc')
      .offset(next)
      .limit(20)
      .groupBy('streams.id', 'users.id')
    return listed
  }
  async show(id) {
    const [shown] = await this.model()
      .select(
        'streams.id',
        'streams.title',
        'streams.description',
        'streams.preview',
        'streams.created_at',
        'streams.updated_at',
        'streams.deleted_at',
        this.knex.raw(
          `json_build_object('id', users.id, 'handle', users.handle, 'avatar', users.avatar, 'created_at', users.created_at) as user`
        )
      )
      .leftJoin('users', 'streams.user_id', '=', 'users.id')
      .where('streams.user_id', id)
      .orderBy('streams.created_at', 'desc')
      .limit(1)
      .groupBy('streams.id', 'users.id')

    let viewers = await this.redis.hget('viewers', id)
    if (viewers) {
      viewers = JSON.parse(viewers).map(
        ({ ip_address, ...rest }) => rest
      )
    }
    if (shown) {
      shown.viewers = viewers
    }

    return shown
  }
}

