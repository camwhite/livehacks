import fs from 'fs'
import pino from 'pino'
import fastify from 'fastify'
import helmet from 'helmet'
import knex from 'fastify-knex'
import jwt from 'fastify-jwt'
import auth from 'fastify-auth'
import redis from 'fastify-redis'
import rateLimit from 'fastify-rate-limit'
import OpenAI from 'openai-api'

import routes from './routes/index.js'
import sockets from './sockets/index.js'
import {
  knexOptions,
  jwtSecret,
  isProd,
  OPENAI_API_KEY
} from '../config/index.js'
import {
  verifyJwt,
  sanitize,
  Bot,
  getBot
} from './actions/index.js'

//const streamClient = stream.connect(
//getstream.key,
//getstream.secret,
//getstream.appId
//)

const log = pino({ prettyPrint: !isProd })
const app = fastify({
  trustProxy: true,
  logger: {
    logger: log,
    serializers: {
      req(request) {
        return { remoteAddress: request.headers['x-real-ip'] }
      }
    }
  }
})
const openai = new OpenAI(OPENAI_API_KEY)

// Decorators
app.decorate('verifyJwt', verifyJwt)
//app.decorate('stream', streamClient) // stream client

// Middlewares
app.use(helmet())

// Plugins
app.register(jwt, { secret: jwtSecret })
app.register(auth)
app.register(redis)
app.register(knex, knexOptions)
app.after(async () => {
  await sockets(app, log)
  const hax = await getBot(app.knex)
  const bot = new Bot(openai, hax)
  app.decorate('hax', bot)
})
app.register(rateLimit, {
  max: 100,
  timeWindow: '1 minute',
  keyGenerator: function(req) {
    return req.headers['x-real-ip'] || req.raw.ip
  }
})

// Hooks
//app.register(hooks)
app.addHook('preHandler', sanitize)

// Mount the routes
app.register(routes, { prefix: '/api/v1' })

export default app
