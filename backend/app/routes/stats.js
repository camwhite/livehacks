import { Stat } from '../actions/index.js'

export default function(app, opts, next) {
  const stat = new Stat(app)

  app.get('/:id', async (request, reply) => {
    try {
      const response = await stat.list(request.params.id)
      reply.send(response)
    } catch (err) {
      reply.status(err.statusCode || 500).send(err)
    }
  })

  app.get('/', async (request, reply) => {
    try {
      const response = await stat.all()
      reply.send(response)
    } catch (err) {
      reply.status(err.statusCode || 500).send(err)
    }
  })

  next()
}
