import xss from 'xss'
import { Message, unfurl } from '../actions/index.js'

export default function(app, opts, next) {
  const message = new Message(app.knex)

  app.get('/:id', async (request, reply) => {
    try {
      const listed = await message.list(
        request.params.id,
        request.query.next
      )
      reply.send(listed)
    } catch (err) {
      reply.status(err.statusCode || 500).send(err)
    }
  })

  app.post('/task', async (request, reply) => {
    try {
      const response = await app.hax.taskord(request)
      reply.status(201).send()
    } catch (err) {
      reply.status(err.statusCode || 500).send(err)
    }
  })

  app.post('/', async (request, reply) => {
    try {
      const created = await message.create(request.body)
      global.io.sockets
        .to(request.body.room_id)
        .emit('messageCreated', created)
      const completion = await app.hax.generate(request.body)
      if (completion.text && !/method/g.test(completion.text)) {
        completion.text = xss(completion.text)
        const created = await message.create(completion)
        global.io.sockets
          .to(request.body.room_id)
          .emit('messageCreated', created)
      }

      const urlRegex = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/
      const [url] = created.text.match(urlRegex) || []
      const attachments = []
      if (url) {
        const attachment = await unfurl(url)
        attachments.push(attachment)
        delete created.user
        const { handle } = created
        delete created.handle
        const response = await message.update(
          {
            ...created,
            attachments: JSON.stringify(attachments)
          },
          { id: created.id }
        )
        global.io.sockets
          .to(response.room_id)
          .emit('messageUpdated', {
            ...response,
            handle
          })
      }

      reply.status(201).send()
    } catch (err) {
      reply.status(err.statusCode || 500).send(err)
    }
  })

  app.put(
    '/:id',
    { preHandler: [app.auth([app.verifyJwt])] },
    async (request, reply) => {
      try {
        const response = await message.update(
          request.body,
          request.params
        )
        global.io.sockets
          .to(response.room_id)
          .emit('messageUpdated', {
            ...response,
            handle: request.user.handle
          })

        reply.status(204).send()
      } catch (err) {
        reply.status(err.statusCode || 500).send(err)
      }
    }
  )

  next()
}
