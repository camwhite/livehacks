import users from './users.js'
import messages from './messages.js'
import streams from './streams.js'
import stats from './stats.js'

export default function(app, opts, next) {
  app.register(users, { prefix: '/users' })
  app.register(messages, { prefix: '/messages' })
  app.register(streams, { prefix: '/streams' })
  app.register(stats, { prefix: '/stats' })

  // Catch all unkown routes
  app.all('*', (request, reply) => {
    reply.status(404).send('Not Found :(')
  })

  next()
}
