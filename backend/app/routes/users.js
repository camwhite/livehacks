import { User } from '../actions/index.js'
import { testUuid } from '../actions/index.js'

export default function(app, opts, next) {
  const user = new User(app.knex)

  app.get(
    '/me',
    { preHandler: [app.auth([app.verifyJwt])] },
    async (request, reply) => {
      reply.send(request.user)
    }
  )

  app.get(
    '/:id',
    { preHandler: [testUuid] },
    async (request, reply) => {
      if (
        request.user &&
        request.user.id === request.params.id
      ) {
        return reply.send(request.user)
      }
      try {
        const shown = await user.show(request.params.id)
        reply.send(shown)
      } catch (err) {
        reply.status(err.statusCode || 500).send(err)
      }
    }
  )

  app.put(
    '/',
    { preHandler: [app.auth([app.verifyJwt])] },
    async (request, reply) => {
      try {
        const updated = await user.update(
          request.body,
          request.user.id
        )
        reply.send(updated)
      } catch (err) {
        reply.status(err.statusCode || 500).send(err)
      }
    }
  )

  next()
}
