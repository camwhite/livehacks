import { Stream } from '../actions/index.js'

export default function(app, opts, next) {
  const stream = new Stream(app)

  app.get('/:id', async (request, reply) => {
    try {
      const shown = await stream.show(request.params.id)
      reply.send(shown)
    } catch (err) {
      reply.status(err.statusCode || 500).send(err)
    }
  })
  app.get('/', async (request, reply) => {
    try {
      const all = await stream.all(request.query.next)
      reply.send(all)
    } catch (err) {
      reply.status(err.statusCode || 500).send(err)
    }
  })
  app.post(
    '/',
    { preHandler: [app.auth([app.verifyJwt])] },
    async (request, reply) => {
      try {
        const created = await stream.create(
          request.body,
          request.user
        )
        global.io.sockets.emit('stream', created)
        reply.send(created)
      } catch (err) {
        reply.status(err.statusCode || 500).send(err)
      }
    }
  )
  app.put(
    '/:id',
    { preHandler: [app.auth([app.verifyJwt])] },
    async (request, reply) => {
      try {
        const updated = await stream.update(
          request.body,
          request.params.id
        )
        reply.send(updated)
      } catch (err) {
        reply.status(err.statusCode || 500).send(err)
      }
    }
  )

  next()
}
