import { Message } from '../actions/index.js'

export default function ({ knex, jwt }, socket, logger) {
  const message = new Message(knex, jwt)
  socket.on('messageCreate', async data => {
    try {
      const created = await message.create(data)
      socket.to(data.room_id).emit('messageCreated', created)
    } catch (err) {
      logger.error(err)
      socket.emit('failure', err)
    }
  })
}
