import { User } from '../actions/index.js'

export default function({ knex, jwt }, socket, logger) {
  const user = new User(knex, jwt)
  socket.on('userUpdate', async data => {
    try {
      const updated = await user.update(data, socket.user.id)
      socket.emit('updateUser', updated)
    } catch (err) {
      logger.error(err)
      socket.emit('failure', err)
    }
  })
}
