import { Auth } from '../actions/index.js'

export default function({ knex, jwt }, socket, logger) {
  const auth = new Auth(knex, jwt)
  socket.on('attempt', async ({ email }) => {
    try {
      const info = await auth.sendMail(email, socket.id)
      socket.emit('email', info)
    } catch (err) {
      logger.error(err)
      socket.emit('failure', err)
    }
  })
  socket.on('login', async accessToken => {
    try {
      const {
        currentUser,
        token,
        socketId
      } = await auth.authenticate(accessToken, socket.ip)
      logger.info(`User ${currentUser.id} logged in ${socketId}`)
      socket.user = { id: currentUser.id, isLoggedIn: true }
      socket.emit('logged', { currentUser, token })
      global.io.sockets
        .to(socketId)
        .emit('logged', { currentUser, token })
    } catch (err) {
      logger.error(err)
      socket.emit('failure', err)
    }
  })
}
