import sockets from 'socket.io'
import auth from './auth.js'
import users from './users.js'
import streams from './streams.js'
import messages from './messages.js'

export default async function(app, logger) {
  const io = sockets(app.server)
  global.io = io
  io.use(async (socket, next) => {
    try {
      const { token } = socket.handshake.query
      if (token) {
        const decoded = await app.jwt.verify(token)
        socket.user = { id: decoded.sub, isLoggedIn: true }
        next()
      } else {
        socket.user = { isLoggedIn: false, id: socket.id }
        next()
      }
    } catch (err) {
      logger.error(err)
      next(err)
    }
  })

  io.on('connection', async socket => {
    socket.ip =
      socket.handshake.headers['x-real-ip'] ||
      socket.request.connection.remoteAddress

    const { id, isLoggedIn } = socket.user
    if (isLoggedIn) {
      logger.info(`User ${id} connected from ${socket.ip}`)
      socket.join(id)
      await app
        .knex('users')
        .update({
          ip_address: socket.ip
        })
        .where({ id })
    } else {
      logger.warn(
        `Unauthenticated socket ${socket.id} connected from ${socket.ip}`
      )
    }

    streams(app, socket, logger)
    messages(app, socket, logger)
    users(app, socket, logger)
    auth(app, socket, logger)
  })
}
