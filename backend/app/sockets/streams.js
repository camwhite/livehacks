import { Stream } from '../actions/index.js'

export default function(app, socket, logger) {
  let currentRoom, isLive
  const { user } = socket
  const stream = new Stream(app)

  socket.on('join', async data => {
    socket.emit('join', socket.id)
    socket.join(data.user.id)
    currentRoom = data.user.id
    if (user.isLoggedIn) {
      logger.info(`User ${user.id} joined room ${currentRoom}`)
    } else {
      logger.warn(
        `Socket ${socket.id} joined room ${currentRoom}`
      )
    }
    const viewers = await stream.addViewer(
      currentRoom,
      user.id,
      socket.ip
    )
    global.io.sockets
      .to(currentRoom)
      .emit('viewerCount', viewers)
  })
  socket.on('leave', async data => {
    socket.leave(currentRoom)
    socket.to(currentRoom).emit('leave', socket.id)
    if (user.isLoggedIn) {
      logger.info(`User ${user.id} left room ${currentRoom}`)
    } else {
      logger.warn(`Socket ${socket.id} left room ${currentRoom}`)
    }
    try {
      const viewers = await stream.removeViewer(
        currentRoom,
        user.id,
        socket.ip
      )
    } catch (err) {
      console.error(err)
    }
    global.io.sockets
      .to(currentRoom)
      .emit('viewerCount', viewers)
    currentRoom = null
  })
  socket.on('live', async ({ user }) => {
    isLive = true
    socket.to(user.id).broadcast.emit('live', socket.id)
    logger.info(`${user.id} went live`)
  })
  socket.on('end', async ({ user, data }) => {
    isLive = false
    socket.to(user.id).emit('leave', user.id)
    logger.info(`${user.handle} ended stream`)
    const deleted = await stream.softDel(user.id)
    global.io.sockets.emit('close', deleted)
  })
  socket.on('viewer', ({ viewerId, streamer }) => {
    if (currentRoom) {
      global.io.sockets.to(streamer.id).emit('peer', viewerId)
      logger.info(
        `${streamer.handle} sent peer info to viewer ${viewerId}`
      )
    }
  })
  socket.on('signal', signal => {
    io.sockets.to(signal.to).emit('signal', signal)
  })
  socket.on('streamUpdate', async data => {
    try {
      const updated = await stream.update(data)
      socket
        .to(updated.user_id)
        .broadcast.emit('updateStream', updated)
    } catch (err) {
      logger.error(err)
      socket.emit('failure', err)
    }
  })
  socket.on('disconnect', async () => {
    if (socket.user.isLoggedIn) {
      logger.info(`User ${user.id} disconnected`)
    } else {
      logger.warn(
        `Unauthenticated socket ${socket.id} disconnected`
      )
    }
    if (currentRoom) {
      if (isLive) {
        await stream.softDel(user.id)
        io.sockets.to(currentRoom).emit('leave', user.id)
      } else {
        io.sockets.to(currentRoom).emit('leave', socket.id)
      }
      const viewers = await stream.removeViewer(
        currentRoom,
        user.id,
        socket.handshake.address
      )
      socket.to(currentRoom).emit('viewers', viewers)
    }
  })
}
