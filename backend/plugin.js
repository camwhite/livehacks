import server from './app'

export default {
  install(app) {
    server(app)
  }
}
